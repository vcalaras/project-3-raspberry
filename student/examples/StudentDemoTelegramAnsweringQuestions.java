package student.examples;

import telegram.TelegramClient;
import telegram.TelegramMessageListener;

import java.io.IOException;

// flo 456062081 - vlad 499163890
import data.Generator;
import data.Sensor;
import data.Sample;
import monitorable.*;
import interfaces.Filter;
import interfaces.Stats;
import java.util.*;

/**
 * @author Maxime Piraux, maxime.piraux@student.uclouvain.be
 * @author Pierre Schaus, pierre.schaus@uclouvain.be
 */
public class StudentDemoTelegramAnsweringQuestions {
    static float temp=0;
    static float bhum=0;
    static float ohum=0;
    static float rhum=0;
    static float chum=0;
    public static void main(String[] args) throws IOException {
        // Création du client Telegram. Vous trouverez plus d'informations sur l'obtention de ces valeurs
        // dans la documentation prévue à cet effet.

        final TelegramClient client = new TelegramClient("projet3_raspberry_bot", "497244417:AAGd-kHGO22cypJSveD12tvzHLAtc7X1Qbc", "456062081");
        // Ajout d'un listener en instanciant une classe anonyme implémentant TelegramMessageListener.
        // Lorsque vous accéder des variables déclarés à l'extérieur d'une classe anonyme, celles-ci doivent être final.
        // Un TelegramMessageListener verra sa méthode messageReceived appelée lorsque l'utilisateur
        // envoie un message au bot sur Telegram.
        Generator.generateFile("random.samples", 1440);
        Sensor sensor = Sensor.getRaspberryPiSensor();
        Bamboo b = new Bamboo(sensor);

        b.addListener(new PlantSampleListener() {
                @Override
                public void sampleTaken(PlantSample sample) {
                    System.out.println(sample.getTime().toDate().toString());
                    System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
                    temp=sample.getTemperature();
                    bhum=sample.getHumidity();
                    if(bhum< b.getLowerBound() +0.5) {
                        try {
                            client.sendMessage("Alerte", "Le niveau d'eau de la plante b est trop faible ("+bhum+") !Elle sera automatiquement arrosée de 20ml");
                            b.water(20);
                        } catch (IOException ex) {
                            // Erreur d'I/O lors de l'envoi du message
                            // Affichage de l'erreur et arret du programme.
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                }
            });

        Orchidea o = new Orchidea(sensor);

        o.addListener(new PlantSampleListener() {
                @Override
                public void sampleTaken(PlantSample sample) {
                    System.out.println(sample.getTime().toDate().toString());
                    System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
                    ohum=sample.getHumidity();
                    if(ohum< o.getLowerBound() +0.5) {
                        try {
                            client.sendMessage("Alerte", "Le niveau d'eau de la plante o est trop faible ("+ohum+") !Elle sera automatiquement arrosée de 20ml");
                            o.water(20);
                        } catch (IOException ex) {
                            // Erreur d'I/O lors de l'envoi du message
                            // Affichage de l'erreur et arret du programme.
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                }
            });

        Rose r = new Rose(sensor);

        r.addListener(new PlantSampleListener() {
                @Override
                public void sampleTaken(PlantSample sample) {
                    System.out.println(sample.getTime().toDate().toString());
                    System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
                    rhum=sample.getHumidity();
                    if(rhum< r.getLowerBound() +0.5) {
                        try {
                            client.sendMessage("Alerte", "Le niveau d'eau de la plante r est trop faible ("+rhum+") !Elle sera automatiquement arrosée de 20ml");
                            r.water(20);
                        } catch (IOException ex) {
                            // Erreur d'I/O lors de l'envoi du message
                            // Affichage de l'erreur et arret du programme.
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                }
            });

        Cactus c = new Cactus(sensor);

        c.addListener(new PlantSampleListener() {
                @Override
                public void sampleTaken(PlantSample sample) {
                    System.out.println(sample.getTime().toDate().toString());
                    System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
                    chum=sample.getHumidity();
                    if(chum< c.getLowerBound() +0.5) {
                        try {
                            client.sendMessage("Alerte", "Le niveau d'eau de la plante c est trop faible ("+chum+") !Elle sera automatiquement arrosée de 20ml");
                            c.water(20);
                        } catch (IOException ex) {
                            // Erreur d'I/O lors de l'envoi du message
                            // Affichage de l'erreur et arret du programme.
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                }
            });

        sensor.start();

        // Tàches à faire 
        // remplir la serre de plantes 
        // ajouter le listener
        // 
        try {
            client.sendMessage("Voici les commandes disponibles : ",""
                + "\n" + "\n" +"/bHumidity , Affiche le taux d'humidité de la plante b" + "\n" +"/bLowerBound , Affiche le taux d'humidité minimal de la plante b" + "\n" +"/bHigherBound , Affiche le taux d'humidité maximal de la plante b"
                + "\n" + "\n" +"/oHumidity , Affiche le Taux d'humidité de la plante o" + "\n" +"/oLowerBound , Affiche le taux d'humidité minimal de la plante o" + "\n" +"/oHigherBound , Affiche le taux d'humidité maximal de la plante o"
                + "\n" + "\n" +"/rHumidity , Affiche le Taux d'humidité de la plante r" + "\n" +"/rLowerBound , Affiche le taux d'humidité minimal de la plante r" + "\n" +"/rHigherBound , Affiche le taux d'humidité maximal de la plante r"
                + "\n" + "\n" +"/cHumidity , Affiche le Taux d'humidité de la plante c" + "\n" +"/cLowerBound , Affiche le taux d'humidité minimal de la plante c" + "\n" +"/cHigherBound , Affiche le taux d'humidité maximal de la plante c"
                + "\n" + "\n" +"/Temperature , Affiche la temperature de la serre actuelle");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        client.addListener(new TelegramMessageListener() {
                @Override
                public void messageReceived(String text)  {

                    // si le message contient un "?" ...
                    if (text.contains("?")) {
                        try {
                            // ... alors on répond avec une réponse simple ;-)
                            client.sendMessage("I have the answer !", "42");

                            // notez que client.pushFile permet d'envoyer des fichiers
                            // par exemple client.pushFile("dayGraph.png", null);
                        } catch (IOException ex) {
                            // Erreur d'I/O lors de l'envoi du message
                            // Affichage de l'erreur et arret du programme.
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }

                    if (text.contains("/bHumidity")) {
                        try {
                            client.sendMessage("résultat :", Float.toString(bhum));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/bLowerBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(b.getLowerBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/bHigherBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(b.getHigherBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/oHumidity")) {
                        try {
                            client.sendMessage("résultat :", Float.toString(ohum));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/oLowerBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(o.getLowerBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/oHigherBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(o.getHigherBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/rHumidity")) {
                        try {
                            client.sendMessage("résultat :", Float.toString(rhum));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/rLowerBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(r.getLowerBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/rHigherBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(r.getHigherBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/cHumidity")) {
                        try {
                            client.sendMessage("résultat :", Float.toString(chum));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/cLowerBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(c.getLowerBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/cHigherBound")) {
                        try {
                            client.sendMessage("résultat :",Float.toString(c.getHigherBound()));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }
                    if (text.contains("/Temperature")) {
                        try {
                            client.sendMessage("résultat :", Float.toString(temp));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            System.exit(-1);
                        }
                    }

                }
            });

    }
}