package student.examples;

import chart.Chart;
import chart.DataSet;
import data.Generator;
import data.Sample;
import data.SampleListener;
import data.Sensor;

import java.io.IOException;

import time.Instant;

/**
 * LSINF1102 P3 Projet demo
 * 
 * Cette demo vise à montrer comment generer un graphique
 * au départ de mesures de température globale de la serre
 *
 * @author Maxime Piraux, maxime.piraux@student.uclouvain.be
 * @author Pierre Schaus, pierre.schaus@uclouvain.be
 */
public class StudentDemoGraph {

    public static void main(String[] args) throws IOException, InterruptedException {
        // Sauvegarde de l'instant présent tronqué à la minute
        final Instant now = Instant.now().truncatedTo(Instant.CHRONO_UNIT_MINUTES);

    	// Generation aléatoire d'un fichier de mesures (de température)
    	// pendant 24h (1440 minutes)  depuis maintenant-24h jusqu'à maintenant.
    	Generator.generateFile("random.samples", 1440);

        // Ouverture par le senseur du fichier généré
        Sensor sensor = Sensor.getSampleReaderSensor("random.samples");

        // Création d'un ensemble de données avec une precision à la minutes
        final DataSet temperature = new DataSet("Temperature", DataSet.PRECISION_MINUTES);

        // Définition d'une classe anonyme qui implémente l'interface SampleListener.
        // Lorsque vous accéder des variables déclarés à l'extérieur d'une classe anonyme, celles-ci doivent être final.
        // La méthode sampleTaken sera appelé au fur et à mesure
        // que le senseur va lire le fichier généré.
        sensor.addListener(new SampleListener() {
            @Override
            public void sampleTaken(Sample sample) {
                // Ajout d'un point (x,y) à l'ensemble de données correspondant à l'échantillon.
                // Les coordonnées sont x = la date en milli seconde,  y = la mesure de temperature globale de la serre.
            	temperature.addPoint(sample.getTime().toDate(), sample.getTemperature());

                // On test si il s'agit du dernier sample du fichier.
                // Le dernier sample du fichier est le sample au temps now - 60 secondes.
                if(now.toEpochSecond() - sample.getTime().toEpochSecond() == 60) {

                    // Création du graphique au départ de l'ensemble des points (x,y)
                    Chart chart = new Chart("Temperature over the last 24 hours", temperature);

                    // Sauvegarde du graphique sous forme de fichier image png avec une résolution 1200x400
                    try {
                        chart.saveChartAsPNG("dayGraph.png", 1200, 400);
                    } catch (IOException e) {
                        // Erreur d'I/O lors de l'écriture du fichier.
                        // Affichage de l'erreur.
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
            }
        });

        // On commence la lecture du fichier, la méthode sampleTaken de notre classe anonyme ci-dessus
        // ne sera appelé qu'après le démarrage du senseur.
        sensor.start();
    }
}
