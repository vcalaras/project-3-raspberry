package student.examples;

import monitorable.*;
import monitorable.PlantSampleListener;

public class PlantSampleListenerImpl implements PlantSampleListener {

    @Override
    public void sampleTaken(PlantSample sample) {
        // TODO Auto-generated method stub
        if(sample.getHumidity() < sample.getPlant().getHigherBound()-0.5){
            sample.getPlant().water(20);
        }
    }

}
