package student.examples;


import data.Generator;
import data.Sensor;
import monitorable.*;
import monitorable.Plant;
import monitorable.PlantSample;
import monitorable.PlantSampleListener;

import java.io.IOException;


/**
 * @author Maxime Piraux, maxime.piraux@student.uclouvain.be
 * @author Pierre Schaus, pierre.schaus@uclouvain.be
 */
public class StudentDemoSensorListener {

    public static void main(String[] args) throws IOException {

        // Generation aléatoire d'un fichier de mesures (température et humidité)
        // pendant 24h (1440 minutes)  depuis maintenant-24h jusqu'à maintenant.
        Generator.generateFile("random.samples", 1440);

        // Ouverture par le senseur du fichier généré
        //Sensor sensor = Sensor.getSampleReaderSensor("random.samples");
        // Ouverture du senseur raspberry
        Sensor sensor = Sensor.getRaspberryPiSensor();

        // Ajout d'un bambou dans la serre
        Bamboo b = new Bamboo(sensor);

        b.addListener(new PlantSampleListener() {
            @Override
            public void sampleTaken(PlantSample sample) {
                 // La méthode sampleTaken est appelée quand une plante émet un sample.
                System.out.println(sample.getTime().toDate().toString());
                System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
            }
        });
        
        Orchidea o = new Orchidea(sensor);

        o.addListener(new PlantSampleListener() {
            @Override
            public void sampleTaken(PlantSample sample) {
                 // La méthode sampleTaken est appelée quand une plante émet un sample.
                System.out.println(sample.getTime().toDate().toString());
                System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
            }
        });
        
        Rose r = new Rose(sensor);

        r.addListener(new PlantSampleListener() {
            @Override
            public void sampleTaken(PlantSample sample) {
                 // La méthode sampleTaken est appelée quand une plante émet un sample.
                System.out.println(sample.getTime().toDate().toString());
                System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
            }
        });
        
        Cactus c = new Cactus(sensor);

        c.addListener(new PlantSampleListener() {
            @Override
            public void sampleTaken(PlantSample sample) {
                 // La méthode sampleTaken est appelée quand une plante émet un sample.
                System.out.println(sample.getTime().toDate().toString());
                System.out.println(sample.getPlant().toString()+" "+sample.getHumidity());
            }
        });




        // On commence la lecture du fichier, la méthode sampleTaken de notre classe PlantListener ci-dessous
        // ne sera appelée qu'après le démarrage du senseur.
        sensor.start();
    }

}