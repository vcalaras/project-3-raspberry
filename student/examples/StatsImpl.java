package student.examples;

import data.Sample;
import interfaces.Filter;
import interfaces.Stats;

public class StatsImpl implements Stats {

    @Override
    public float getAverageTemp(Sample[] samples) {
        // TODO Auto-generated method stub
        float averageTemp=0;
        for(int i=0;i<samples.length;i++){
            averageTemp+=samples[i].getTemperature();
        }
        return averageTemp/samples.length;
    }

    @Override
    public float getAverageHum(Sample[] samples) {
        // TODO Auto-generated method stub
        float averageHum=0;
        for(int i=0;i<samples.length;i++){
            averageHum+=samples[i].getHumidity();
        }
        return averageHum/samples.length;		
    }

    @Override
    public float getMinTemp(Sample[] samples) {
        // TODO Auto-generated method stub
        float minTemp=samples[0].getTemperature();
        for(int i=0;i<samples.length;i++){
            if(samples[i].getTemperature()<minTemp){
                minTemp=samples[i].getTemperature();
            }
        }
        return minTemp;
    }

    @Override
    public float getMinHum(Sample[] samples) {
        // TODO Auto-generated method stub
        float minHum=samples[0].getHumidity();
        for(int i=0;i<samples.length;i++){
            if(samples[i].getHumidity()<minHum){
                minHum=samples[i].getHumidity();
            }
        }
        return minHum;
    }

    @Override
    public float getMaxTemp(Sample[] samples) {
        // TODO Auto-generated method stub
        float maxTemp=samples[0].getTemperature();
        for(int i=0;i<samples.length;i++){
            if(samples[i].getTemperature()>maxTemp){
                maxTemp=samples[i].getTemperature();
            }
        }
        return maxTemp;
    }

    @Override
    public float getMaxHum(Sample[] samples) {
        // TODO Auto-generated method stub
        float maxHum=samples[0].getHumidity();
        for(int i=0;i<samples.length;i++){
            if(samples[i].getHumidity()>maxHum){
                maxHum=samples[i].getHumidity();
            }
        }
        return maxHum;
    }

    @Override
    public float getVarianceTemp(Sample[] samples) {
        // TODO Auto-generated method stub
        float varianceTemp=0;
        for(int i=0;i<samples.length;i++){
            varianceTemp+=(samples[i].getTemperature()-getAverageTemp(samples))*(samples[i].getTemperature()-getAverageTemp(samples));
        }
        return varianceTemp/samples.length;
    }

    @Override
    public float getVarianceHum(Sample[] samples) {
        // TODO Auto-generated method stub
        float varianceHum=0;
        for(int i=0;i<samples.length;i++){
            varianceHum+=(samples[i].getHumidity()-getAverageHum(samples))*(samples[i].getHumidity()-getAverageHum(samples));
        }
        return varianceHum/samples.length;
    }

    @Override
    public float getStandardDeviationTemp(Sample[] samples) {
        // TODO Auto-generated method stub
        return (float)Math.sqrt(getVarianceTemp(samples));
    }

    @Override
    public float getStandardDeviationHum(Sample[] samples) {
        // TODO Auto-generated method stub
        return (float)Math.sqrt(getVarianceHum(samples));
    }

    @Override
    public float correlationTempHum(Sample[] samples) {
        // TODO Auto-generated method stub
        float correlation=0;
        float covariance=0;
        for(int i=0;i<samples.length;i++){
            covariance+=(samples[i].getTemperature()-getAverageTemp(samples))*(samples[i].getHumidity()-getAverageHum(samples));
        }
        covariance/=samples.length;
        correlation=covariance/(getStandardDeviationTemp(samples)*getStandardDeviationHum(samples));		
        return correlation;
    }

    @Override
    public Sample[] filter(Sample[] samples, Filter filter) {
        // TODO Auto-generated method stub
        int newTabLength=0;
        for(int i=0;i<samples.length;i++){
            if(filter.matchedBy(samples[i]) == true){
                newTabLength+=1;
            }
        }
        Sample[] samples2=new Sample[newTabLength];
        int a=0;
        for(int b=0;b<samples.length;b++){
            if(filter.matchedBy(samples[b]) == true){
                samples2[a]=samples[b];
                a++;
            }
        }
        return samples2;
    }

}
