package student.linearregression;
public class LinearRegression {

    /**
     * @param x the x coordinates
     * @param y the y coordinates i.e. point i is defined as (x[i],y[i])
     * @param alpha the learning rate of the gradient descent
     *      (a small positive value, typically 0.001)
     * @param iter  the number of iteration of the gradient descent algorithm
     * @return a line equation that fits at best (least-square error) the given points using the "gradient descent algorihtm" initialized with m=0 (slope), b=0 (intercept).
     */
    public static StraightLine fitLine(double [] x, double [] y, double alpha, int iter) {
        // TODO 
        double xAverage=0;
        for(int i=0;i<x.length;i++){
            xAverage+=x[i];
        }
        xAverage/=x.length;

        double yAverage=0;
        for(int i=0;i<y.length;i++){
            yAverage+=y[i];
        }
        yAverage/=y.length;

        double xcorrsum=0;
        for(int i=0;i<x.length;i++){
            xcorrsum+=Math.pow((x[i]-xAverage),2);
        }

        double ycorrsum=0;
        for(int i=0;i<y.length;i++){
            ycorrsum+=Math.pow((y[i]-yAverage),2);
        }

        double sum=0;
        for(int i=0;i<x.length;i++){
            sum+=(x[i]-xAverage)*(y[i]-yAverage);
        }

        double correlationCoefficient=sum/Math.sqrt(xcorrsum*ycorrsum);

        double sumx=0;
        for(int i=0;i<x.length;i++){
            sumx+=x[i];
        }

        double sumx2=0;
        for(int i=0;i<x.length;i++){
            sumx2+=x[i]*x[i];
        }

        double sumy=0;
        for(int i=0;i<y.length;i++){
            sumy+=y[i];
        }        

        double sumy2=0;
        for(int i=0;i<y.length;i++){
            sumy2+=y[i]*y[i];
        }

        double sumxy=0;
        for(int i=0;i<x.length;i++){
            sumxy+=x[i]*y[i];        
        }

        double b=(((iter*sumxy)-(sumx*sumy))/(iter*sumx2-Math.pow(sumx,2)));

        double a=yAverage-b*xAverage;
        return new StraightLine(b,a);
    }
}