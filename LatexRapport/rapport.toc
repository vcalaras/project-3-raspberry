\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Cahier des charges}{3}
\contentsline {subsection}{\numberline {2.1}Fonctionnalit\IeC {\'e}s}{3}
\contentsline {subsection}{\numberline {2.2}Performances}{3}
\contentsline {subsection}{\numberline {2.3}Contraintes}{3}
\contentsline {section}{\numberline {3}Test effectu\IeC {\'e}}{4}
\contentsline {section}{\numberline {4}Solution et challenge }{5}
\contentsline {section}{\numberline {5} Introduction au Raspberry Pi du point de vu mat\IeC {\'e}riel}{6}
\contentsline {section}{\numberline {6}Manuel d\IeC {\textquoteright }utilisation du bot et de ses commandes}{7}
\contentsline {section}{\numberline {7}Conclusion}{8}
\contentsline {section}{\numberline {8}Planning}{9}
\contentsline {subsection}{\numberline {8.1}Description des t\IeC {\^a}ches}{9}
\contentsline {subsection}{\numberline {8.2}Ordonnancement des t\IeC {\^a}ches}{10}
\contentsline {subsection}{\numberline {8.3}Diagramme de Gantt}{11}
